FROM amazoncorretto:17
COPY ./target/main-0.0.1-SNAPSHOT.jar /usr/local/lib/main.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/usr/local/lib/main.jar"]