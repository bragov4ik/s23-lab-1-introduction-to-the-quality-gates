# Lab 1 -- Introduction to the quality gates

[![pipeline status](https://gitlab.com/bragov4ik/lab-1-introduction-to-the-quality-gates/badges/main/pipeline.svg)](https://gitlab.com/bragov4ik/lab-1-introduction-to-the-quality-gates/-/commits/main)

## Homework

Deployment is done via publising image built in CI to gitlab container registry and pulling the update with Watchtower.

### Showcase

The server is running in innopolis LAN at http://10.90.138.135:8080/hello

Running server
![](https://i.imgur.com/5EgkhKJ.png)
![](https://i.imgur.com/hNWKPMq.png)
Pipeline creates an image and pushes it to registry
![](https://i.imgur.com/Q0ObjDu.png)
![](https://i.imgur.com/y6j1yjE.png)
Watchtower automatically updates the image and restarts the container
![](https://i.imgur.com/zusJC5K.png)
Shazam!11
![](https://i.imgur.com/BAo9Hxn.png)
